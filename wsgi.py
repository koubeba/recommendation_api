from flask import Flask, request, abort
from flask_restful import Resource, Api
from prediction.model import Model
import pandas
import os

VIEWED_PRODUCTS_KEY = "viewed_products"

app = Flask(__name__)
api = Api(app)
model = Model()

@app.route('/NNrecommendations', methods=['POST'])
def recommendNN():
    data = request.json
    if data is None:
        abort(400)
    if VIEWED_PRODUCTS_KEY in data.keys(): 
        return {"recommendations": model.predictNN(data[VIEWED_PRODUCTS_KEY])}
    else:
        abort(400)

@app.route('/CFrecommendations', methods=['POST'])
def recommendCF():
    data = request.json
    if data is None:
        abort(400)
    if VIEWED_PRODUCTS_KEY in data.keys(): 
        return {"recommendations": model.predictCF(data[VIEWED_PRODUCTS_KEY])}
    else:
        abort(400)


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(port=port)