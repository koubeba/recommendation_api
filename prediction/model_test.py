from spotlight.sequence.implicit import ImplicitSequenceModel
from spotlight.interactions import Interactions
import unittest
import torch
import os
import pandas

class LSTM_Model(unittest.TestCase):

    def test_tensor_size_should_equal_products_count(self):
        lstm_model = torch.load(('{path}/LSTM_1.1.0').format(path=os.getcwd()))
        self.assertEqual(len(lstm_model.predict([1,2])), 305)

    def test_tensor_should_contain_valid_products_ids(self):
        input_products  = [6, 3, 4]
        products_csv    = pandas.read_csv(('{path}/products.csv').format(path=os.getcwd()))
        product_IDS     = products_csv['product_id_2'].unique().tolist()
        lstm_model      = torch.load(('{path}/LSTM_1.1.0').format(path=os.getcwd()))
        predictions     = lstm_model.predict(input_products)
        best5           = predictions.argsort()[(-1)*5:][::-1].tolist()
        decoded         = [products_csv[products_csv['product_id_cat_2']==cat]['product_id_2'].tolist()[0] for cat in best5]
        self.assertTrue(all(productID in product_IDS for productID in decoded))

if __name__ == '__main__':
    unittest.main()
