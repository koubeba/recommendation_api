import pandas
import pickle
import sys
import torch
from random import sample
import os

class Model:
    def __init__(self):
        self.products_csv   = pandas.read_csv(('{path}/prediction/products.csv').format(path=os.getcwd()))
        self.products_list  = self.products_csv['product_id_2'].tolist()
        self.knn_model      = pickle.load(open(('{path}/prediction/knn_model.pickle').format(path=os.getcwd()), "rb")) 
        self.csr_matrix     = pickle.load(open(('{path}/prediction/csr_matrix.pickle').format(path=os.getcwd()), "rb"))
        self.product_map    = pickle.load(open(('{path}/prediction/product_map.pickle').format(path=os.getcwd()), "rb"))
        self.k = 5

        self.lstm_model = torch.load(('{path}/prediction/LSTM_1.1.0').format(path=os.getcwd()))

    # Jeżeli historia użytkownika jest pusta, poleć losowe produkty
    def predictRandom(self):
        return sample(self.products_list, self.k)

    def predictNN(self, sequence):
        if len(sequence) > 0:
            encoded_seq = [self.products_csv[self.products_csv['product_id_2']==int(product)]['product_id_cat_2'].tolist()[0] for product in sequence]
            predictions = self.lstm_model.predict(encoded_seq)
            best3       = predictions.argsort()[(-1)*self.k:][::-1].tolist()
            decoded     = [self.products_csv[self.products_csv['product_id_cat_2']==cat]['product_id_2'].tolist()[0] \
                            for cat in best3]
            return decoded
        else:
            return self.predictRandom()

        
    def predictCF(self, products):
        if len(products)>0:
            p_dict = {i: self.product_map[i] for i in range(0, len(self.product_map.tolist()))}
            reversed_p_dict = {value: key for (key, value) in p_dict.items()}
            raw_recommends = pandas.DataFrame(
                {'product_id': pandas.Series([], dtype='int64'), 'distance': pandas.Series([], dtype='float64')})
        
            for product in products:
                idx = reversed_p_dict[product]
                distances, indices = self.knn_model.kneighbors(self.csr_matrix[idx])
                rec = pandas.DataFrame({'product_id': pandas.Series(map(lambda x: p_dict[x], indices[0].tolist())),
                                    'distance': pandas.Series(distances[0])}).drop(0)
                raw_recommends = raw_recommends.append(rec, ignore_index=True)
        
            factor = len(products) + 1
            product_count = raw_recommends.groupby('product_id', as_index=False).count()
            product_dist = raw_recommends.groupby('product_id', as_index=False).mean()
            product_dist = pandas.merge(product_dist, product_count, on='product_id')
            product_dist["new_distance"] = product_dist.distance_x * (factor - product_dist.distance_y)
            return product_dist.sort_values(by="new_distance").product_id.head(self.k).tolist()
        else:
            return self.predictRandom()